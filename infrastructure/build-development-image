#! /bin/bash
# vim: set fenc=utf-8 ff=unix sts=2 sw=2 et ft=sh :

# Copyright (C) 2016-2021 Philip H. Smith

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

set -e -u -o pipefail

cd "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"/..

DEVELOPMENT_VERSION=$(
  if git diff-index --quiet HEAD infrastructure/provision-development-image
  then
    git log -1 --format=%h infrastructure/provision-development-image
  else
    echo dirty
  fi
)

docker build \
  -t waylang/development:${DEVELOPMENT_VERSION} \
  --build-arg DEVELOPMENT_VERSION=${DEVELOPMENT_VERSION} \
  infrastructure

if [[ $DEVELOPMENT_VERSION != dirty ]]
then
  docker push waylang/development:${DEVELOPMENT_VERSION}
fi
